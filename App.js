import React, { useEffect } from 'react';
import { Platform, Text } from 'react-native';
import * as Linking from "expo-linking";

export default function App() {

    const url = Linking.useURL();

    const handleURL = (url) => {
        const { hostname, path, queryParams } = Linking.parse(url);
        if (path === 'alert') {
            alert(queryParams.str);
        } else {
            console.log(path, queryParams);
        }
    }

    useEffect(() => {
        // Do something with URL
        if (url) {
            handleURL(url);
        } else {
            console.log('No URL');
        }
    }, [url])

    return <Text>Hello from Home!</Text>;
}


