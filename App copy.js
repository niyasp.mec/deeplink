import React, { useEffect } from 'react';
import { Platform, Text } from 'react-native';
import * as Linking from "expo-linking";

function Home() {
    const handleOpenURL = (event) => {
        alert(event)
        // navigate(event.url);
    };

    // const navigate = (url) => {
    //     if (url) {
    //         const route = url.replace(/.*?:\/\//g, '');
    //         const id = route.match(/\/([^\/]+)\/?$/);
    //         if (id) {
    //             const idValue = id[1];
    //             const routeName = route.split('/')[0];
    //             if (routeName === 'people') {
    //                 navigation.navigate('People', { id: idValue, name: 'chris' });
    //             }
    //         }
    //     }
    // };

    useEffect(() => {
        if (Platform.OS === 'android') {
            Linking.getInitialURL().then(url => {
                alert(url)
                //console.log("first")
                //      navigate(url);
            });
        } else {
            Linking.addEventListener('url', handleOpenURL);

        }

        // return () => {
        //     Linking.removeEventListener('url', handleOpenURL);
        // };
    }, []);

    return <Text>Hello from Home!</Text>;
}

export default Home;
